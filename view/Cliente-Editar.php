<?php

?>
<h1>
    <?php 

	if ($data->id != null){
        echo "Cliente con ID: ". $data->id;
    }else{
        echo "Registrar nuevo cliente";
    } 
    ?>
</h1>


<form id="frm-Cliente" action="?controller=Cliente&accion=add" method="post">
    <input type="hidden" name="id" value="<?php echo $data->id; ?>" />
    
    <div class="form-group">
        <label>Nombres</label>
        <input type="text" name="nombres" value="<?php echo $data->nombres; ?>"  />
    </div>
    
    <div class="form-group">
        <label>Teléfono</label>
        <input type="text" name="telefono" value="<?php echo $data->telefono; ?>"  />
    </div>

    <div class="form-group">
        <label>Email</label>
        <input type="text" name="email" value="<?php echo $data->email; ?>"  />
    </div>

    <div class="form-group">
        <label>Dirección</label>
        <input type="text" name="direccion" value="<?php echo $data->direccion; ?>"  />
    </div>

    <div class="form-group">
        <label>Fecha de registro</label>
        <input type="date" name="registrado" value="<?php echo $data->registrado; ?>"  />
    </div>

    

    
    <hr />
    
    <div>
        <button>Guardar</button>
    </div>
</form>
