<?php

?>
<center>
<h1 class="page-header">Clientes</h1>

<div>
    <a href="?controller=Cliente&accion=Crud">Nuevo Cliente</a>
</div>
<br><br>
<table width="100%" border="2" bordercolor="#000000">
        <tr>
            <th>Id</th>
            <th>Nombres</th>
            <th>Teléfono</th>
            <th>Email</th>
            <th>Dirección</th>
            <th>Fecha de registro</th>

        </tr>
        </center>
    <tbody>
	<?php foreach($this->model->getAll() as $r): ?>
        <tr>
	    <td><?php echo $r->id; ?></td>
            <td><?php echo $r->nombres; ?></td>
            <td><?php echo $r->telefono; ?></td>
            <td><?php echo $r->email; ?></td>
            <td><?php echo $r->direccion; ?></td>
            <td><?php echo $r->registrado; ?></td>
            <td>
                <a href="?controller=Cliente&accion=Crud&id=<?php echo $r->id; ?>">Editar</a>
            </td>
            <td>
               <a href="?controller=Cliente&accion=Del&id=<?php echo $r->id; ?>">Eliminar</a>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table> 
