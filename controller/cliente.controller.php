<?php

require_once 'model/Cliente.php';

class ClienteController{
    
    private $model;
    
    public function __construct(){
        $this->model = new Cliente();
    }    
    public function Index(){
        require_once 'view/Cliente.php';
       
    }    
    public function Crud(){
        $data = new Cliente();
        
        if(isset($_REQUEST['id'])){
            $data = $this->model->getByID($_REQUEST['id']);
        }       
        require_once 'view/Cliente-Editar.php';          
    }  
    public function add(){
        $data = new Cliente();
        
        $data->id = $_REQUEST['id'];
        $data->nombres = $_REQUEST['nombres'];
        $data->telefono = $_REQUEST['telefono'];
        $data->email = $_REQUEST['email'];
        $data->direccion = $_REQUEST['direccion'];
        $data->registrado = $_REQUEST['registrado'];

        $data->id > 0 
            ? $this->model->update($data)
            : $this->model->add($data);        
        header('Location: IndexCliente.php');
    }
    
   
}

?>