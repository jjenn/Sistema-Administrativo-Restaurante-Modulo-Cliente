<?php

include_once("database.php");

class Cliente{
    
    private $pdo;    
    public $id;
    public $nombres;
    public $telefono;
    public $email;
    public $direccion;
    public $registrado;

	public function __construct(){
		try{
			$this->pdo = Database::Conectar();
		}
		catch(Exception $e){
			die($e->getMessage());
		}
	}
	public function getAll()
	{
		try{
			$result = array();
			$stm = $this->pdo->prepare("SELECT id, nombres, telefono, email, direccion, registrado FROM clientes");

			$stm->execute();
			return $stm->fetchAll(PDO::FETCH_OBJ);
		}
		catch(Exception $e){
			die($e->getMessage());
		}
	}

	public function getByID($id)
	{
		try{
			$stm = $this->pdo
			          ->prepare("SELECT id, nombres, telefono, email, direccion, registrado FROM clientes WHERE id = ?");
			          

			$stm->execute(array($id));
			return $stm->fetch(PDO::FETCH_OBJ);
		} catch (Exception $e){
			die($e->getMessage());
		}
	}

	public function del($data)
	{
		try{
			$stm = $this->pdo
			            ->prepare("DELETE FROM clientes WHERE id = ?");			          

			$stm->execute(array($data->id));
		} catch (Exception $e){
			die($e->getMessage());
		}
	}

	public function update($data)
	{
		try{
			$sql = "UPDATE clientes SET 
						nombres = ?,
						telefono = ?,
						email = ?,
						direccion = ?,
						registrado = ?
				    WHERE id = ?";

			$this->pdo->prepare($sql)
			     ->execute(
				    array( 
        $data->nombres,
        $data->telefono,
        $data->email,
        $data->direccion,
        $data->registrado,
        $data->id
					)
				);
		} catch (Exception $e){
			die($e->getMessage());
		}
	}

	public function add(Cliente $data)
	{
		try{
		$sql = "INSERT INTO clientes(nombres,telefono,email,direccion,registrado) 
		        VALUES (?, ?, ?, ?, ?)";

		$this->pdo->prepare($sql)
		     ->execute(
				array(
                    $data->nombres,
                    $data->telefono,
                    $data->email, 
                    $data->direccion, 
                    $data->registrado
                )
			);
		} catch (Exception $e){
			die($e->getMessage());
		}
	}
}

?>